﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace StartStop
{
    // Klasa reprezentująca interwał aktywności zadania.
    // Implementuje interfejs ISerializable, aby można było obiekty tej klasy
    // serializować i deserializować.
    [Serializable()]
    class Interval : ISerializable
    {
        // Czas rozpoczęcia pracy nad zadaniem.
        private DateTime start;

        // Czas zakończenia pracy nad zadaniem.
        private DateTime stop;

        // Konstruktor.
        // Datę startu ustawiamy na 'teraz',
        // a datę końca na DateTime.MinValue,
        // co będziemy traktować jak brak daty końca.
        public Interval()
        {
            start = DateTime.Now;
            stop = DateTime.MinValue;
        }

        // Konstruktor na potrzeby deserializacji.
        public Interval(SerializationInfo info, StreamingContext context)
        {
            this.start = (DateTime)info.GetValue("Start", typeof(DateTime));
            this.stop = (DateTime)info.GetValue("Stop", typeof(DateTime));
        }

        // Przygotowanie składowych do serializacji.
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Start", this.start);
            info.AddValue("Stop", this.stop);
        }

        // Sprawdza, czy interwał jest 'zatrzymany' (czy ma datę końca po dacie startu).
        public bool isStopped()
        {
            return (stop >= start);
        }

        // Zatrzymanie interwału.
        // Jeśli nie był już zatrzymany, ustawiamy datę końca na 'teraz'.
        public void stopIt()
        {
            // Ten warunek może być spełniony tylko jeśli
            // data końca jest równa DateTime.MinValue,
            // czyli wtedy, gdy interwał nie jest jeszcze zatrzymany.
            if (stop < start)
            {
                stop = DateTime.Now;
            }
        }

        // Zwraca datę startu.
        public DateTime getStart()
        {
            return start;
        }

        // Zwraca datę końca.
        public DateTime getStop()
        {
            return stop;
        }

        // Zwraca czas trwania interwału.
        public TimeSpan getDuration()
        {
            if (stop < start)
            {
                // Jeśli interwał nie jest zamknięty,
                // zwracamy czas od startu do 'teraz'.
                return DateTime.Now - start;
            }
            return stop - start;
        }

        // Przeciążona metoda ToString.
        // Z tej metody korzystają m.in. okna list, aby wyświetlić
        // przechowywane elementy. Zmieniamy domyślny sposób,
        // aby interwał na liście pokazywany było jako: 
        //  Start: YYYY-MM-DD hh:mm:ss; Stop: YYYY-MM-DD hh:mm:ss
        override public String ToString()
        {
            String s = "Start: " + start.ToString();
            if (stop >= start)
            {
                s += "; Stop: " + stop.ToString();
            }
            return s;
        }
    }
}
