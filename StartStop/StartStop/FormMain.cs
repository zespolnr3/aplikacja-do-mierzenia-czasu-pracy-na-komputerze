﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace StartStop
{
    // Klasa głównego okna programu.
    public partial class FormMain : Form
    {
        // Lista zadań.
        private List<Task> tasks;

        // Aktualnie wykonywane zadanie.
        private Task currentTask;

        // Konstruktor (tworzony automatycznie).
        public FormMain()
        {
            InitializeComponent();
        }

        // Metoda wykonywana w momencie utworzenia okna.
        private void FormMain_Load(object sender, EventArgs e)
        {
            // Przypisujemy ikonę z zasobów programu jako ikonę programu i ikonę na trayu.
            Icon = Resources.Icon_32;
            notifyIcon.Icon = Resources.Icon_32;

            // Wykonujemy deserializację (odczytanie z pliku) zapisanego stanu aplikacji.
            // Stan aplikacji odczytywany jest z pliku "startstop.ser",
            // który powinien być umieszczony w tym samym katalogu, co program.
            // Elementy serializowane zdefiniowaliśmy w pomocniczej klasie SerializedObjects,
            // i są to: lista zadań (tasks) i aktualnie trwające zadanie (currentTask).
            try
            {
                SerializedObjects so;
                Stream stream = File.Open("startstop.ser", FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                so = (SerializedObjects)bFormatter.Deserialize(stream);
                stream.Close();

                tasks = so.tasks;
                currentTask = so.currentTask;
            }
            catch (Exception)
            {
                // Jeśli wystąpi wyjątek (np. brak pliku "startstop.ser"),
                // program zacznie z pustą listą.
                tasks = new List<Task>();
            }

            // Podpinamy listę zadań jako źródło rekodrów okna listy zadań.
            // Jeśli lista nie jest pusta, w oknie pojawią się wpisy.
            listBoxTasks.DataSource = tasks;
            if (currentTask != null)
            {
                // Jeśli liczymy aktualnie czas jakiegoś zadania,
                // Wyświetlamy w oknie nazwę tego zadania i uruchamiamy timer,
                // który co sekundę aktualizuje wyświetlany czas trwania.
                // Udostępniamy również przycisk "Stop", aby można było zatrzymać zadanie.
                labelCurrentTask.Text = currentTask.getName();
                buttonStop.Enabled = true;
                timer.Start();
            }
        }

        // Metoda uruchamiana w momencie zmiany rozmiaru okna aplikacji.
        private void FormMain_Resize(object sender, EventArgs e)
        {
            // Jeśli okno zostało zminimalizowane, zatrzymaj timer (o ile jest wystartowany),
            // pokaż ikonę na trayu i ukryj główne okno.
            if (FormWindowState.Minimized == this.WindowState)
            {
                if (currentTask != null)
                {
                    timer.Stop();
                }
                notifyIcon.Visible = true;
                this.Hide();
            }
            // Jeśli okno zostało przywrócone, wystartuj timer (jeśli trzeba)
            // i ukryj ikonę w trayu.
            else if (FormWindowState.Normal == this.WindowState)
            {
                if (currentTask != null)
                {
                    timer.Start();
                }
                notifyIcon.Visible = false;
            }
        }

        // Metoda uruchamiana gdy wykonamy dwuklik na ikonie w trayu.
        // Pokazuje główne okno programu i pryzwraca je do normalnego rozmiaru.
        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        // Metoda uruchamiana, gdy wciśnięty zostanie przycisk "Dodaj zadanie".
        // Wyświetla dialog z prośbą o podanie nazwy zadania. Jeśli podano nazwę i wciśnięto "OK",
        // nowe zadanie o podanej nazwie dodawane jest do listy zadań na ostatniej pozycji.
        // Źródło danych okna listy jest aktualizowane, aby dodane zadanie wyświetliło się.
        // Dodane zadanie jest zaznaczane. Na wszelki wypadek udostępniamy przycisk "Start"
        // (mógł być wcześniej wyszarzony).
        private void buttonAddTask_Click(object sender, EventArgs e)
        {
            String taskName = Microsoft.VisualBasic.Interaction.InputBox("Podaj nazwę zadania.", "Nazwa zadania", "");
            if (taskName != null && taskName.Length > 0)
            {
                tasks.Add(new Task(taskName));
                listBoxTasks.DataSource = null;
                listBoxTasks.DataSource = tasks;
                listBoxTasks.SetSelected(listBoxTasks.Items.Count - 1, true);
                buttonStart.Enabled = true;
            }
        }

        // Metoda uruchamiana, gdy wciśnięty zostanie przycisk "Usuń zadanie".
        // Wyświetla dialog z prośbą potwierdzenia operacji usunięcia.
        // Jeśli wciśnięto "Tak", z listy zadań usuwane jest zadanie zaznaczone w oknie
        // z listą zadań, po czym źródło danych okna listy jest aktualizowane.
        private void buttonDeleteTask_Click(object sender, EventArgs e)
        {
            Task task = (Task)listBoxTasks.SelectedItem;
            DialogResult result = MessageBox.Show(
                this, 
                "Czy na pewno chcesz usunąć zadanie '" + task.getName() + "'?", 
                "Potwierdzenie usunięcia zadania", 
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                tasks.Remove(task);
                listBoxTasks.DataSource = null;
                listBoxTasks.DataSource = tasks;
            }
        }

        // Metoda uruchamiana, gdy zmienia się zaznaczenie w oknie z listą zadań.
        // Jeśli zaznaczone jest jakieś zadanie, aktualizujemy źródło danych okna
        // z listą interwałów, podając listę z zaznaczonego zadania.
        // Ustawiamy również odpowiednio dostępność przycisków "Dodaj" i "Usuń.
        private void listBoxTasks_SelectedValueChanged(object sender, EventArgs e)
        {
            listBoxTimes.DataSource = null;
            if (listBoxTasks.SelectedItem == null)
            {
                buttonStart.Enabled = false;
                buttonDeleteTask.Enabled = false;
            }
            else 
            {
                listBoxTimes.DataSource = ((Task)listBoxTasks.SelectedItem).getIntervals();
                buttonStart.Enabled = true;
                buttonDeleteTask.Enabled = true;
            }
        }

        // Metoda uruchamiana, gdy zmienia się dostępność przycisku "Start".
        // Gdy przycisk jest aktywowany, zmieniamy mu kolor na zielony.
        // Gdy jest deaktywowany - na szary.
        private void buttonStart_EnabledChanged(object sender, EventArgs e)
        {
            if (buttonStart.Enabled)
            {
                buttonStart.BackColor = Color.LimeGreen;
            }
            else
            {
                buttonStart.BackColor = Color.Silver;
            }
        }

        // Metoda uruchamiana, gdy zmienia się dostępność przycisku "Stop".
        // Gdy przycisk jest aktywowany, zmieniamy mu kolor na czerwony.
        // Gdy jest deaktywowany - na szary.
        private void buttonStop_EnabledChanged(object sender, EventArgs e)
        {
            if (buttonStop.Enabled)
            {
                buttonStop.BackColor = Color.Crimson;
            }
            else
            {
                buttonStop.BackColor = Color.Silver;
            }
        }

        // Metoda uruchamiana gdy wciśnięty zostaje przycisk "Start".
        // Jeśli jest już mierzony czas jakiegoś zadania, zadanie to jest zatrzymywane
        // i przenoszone z powrotem do listy zadań.
        // Zadanie zaznaczone w oknie z listą zadań przenoszone jest z listy do zmiennej
        // currentTask, a źródło danych okna listy jest aktualizowane.
        // Wybrane zadanie jest następnie 'uruchamiane' (zaczynamy liczyć mu czas),
        // wyświetlamy jego nazwę w oknie aplikacji, aktywujemy przycisk "Stop"
        // i uruchamiamy timer, który będzie co sekundę aktualizował wyświetlany czas trwania zadania.
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (currentTask != null)
            {
                currentTask.stop();
                tasks.Add(currentTask);
            }
            currentTask = (Task)listBoxTasks.SelectedItem;
            tasks.Remove(currentTask);
            listBoxTasks.DataSource = null;
            listBoxTasks.DataSource = tasks;

            currentTask.start();
            labelCurrentTask.Text = currentTask.getName();
            buttonStop.Enabled = true;

            timer.Start();
        }

        // Metoda uruchamiana gdy wciśnięty zostaje przycisk "Stop".
        // Zatrzymujemy liczenie czasu dla aktualnego zadania i przenosimy je
        // z powrotem na listę zadań (aktualizujemy źródło danych okna listy,
        // żeby zmiana była widoczna). Ustawiamy tekst wyświetlany jako nazwa
        // zadania i jego czas na "---" i deaktywujemy przycisk "Stop"
        // (nie ma w tej chwili czego zatrzymywać).
        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (currentTask != null)
            {
                timer.Stop();

                currentTask.stop();
                tasks.Add(currentTask);
                currentTask = null;
                listBoxTasks.DataSource = null;
                listBoxTasks.DataSource = tasks;
            }
            labelCurrentTask.Text = "---";
            labelTotalTime.Text = "---";
            labelTodaysTime.Text = "---";
            buttonStop.Enabled = false;
        }

        // Metoda wykonywana co sekundę przez uruchomiony timer.
        // Wyświetla w oknie aplikacji tekst odpowiadający czasowi trwania
        // aktualnego zadania (całkowity i w danym dniu).
        private void timer_Tick(object sender, EventArgs e)
        {
            labelTotalTime.Text = currentTask.getDuration().ToString(@"dd\.hh\:mm\:ss");
            labelTodaysTime.Text = currentTask.getDurationToday().ToString(@"dd\.hh\:mm\:ss");
        }

        // Metoda wykonywana podczas zamykania okna głównego.
        // Wykonujemt serializację danych aplikacji (listy zadań i aktualnego zadania).
        // Wykorzystujemy pomocniczą klasę SerializedObjects, do której podpinamy 
        // składowe tasks i current Task. Obiekt klasy SerializedObjects zapisywany
        // jest w formie binarnej w pliku "startstop.ser", w tym samym katalogu,
        // z którego uruchomiono aplikację.
        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            SerializedObjects so = new SerializedObjects();
            so.tasks = tasks;
            so.currentTask = currentTask;
            Stream stream = File.Open("startstop.ser", FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, so);
            stream.Close();
        }
    }
}
