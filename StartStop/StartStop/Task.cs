﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace StartStop
{
    // Klasa reprezentująca zadanie.
    // Implementuje interfejs ISerializable, aby można było obiekty tej klasy
    // serializować i deserializować.
    [Serializable()]
    class Task : ISerializable
    {
        // Zazwa zadania.
        private String name;

        // Lista interwałów czasowych, w których zadanie było wykonywane.
        private List<Interval> intervals;

        // Całkowity czas trwania zadania (suma długości zamkniętych interwałów).
        private TimeSpan duration;

        // Konstruktor zadania o podanej nazwie.
        // Zaraz po utworzeniu zadanie ma pustą listę interwałów
        // i zerowy czas wykonywania.
        public Task(String name)
        {
            this.name = name;
            intervals = new List<Interval>();
            duration = TimeSpan.Zero;
        }

        // Konstruktor na potrzeby deserializacji.
        public Task(SerializationInfo info, StreamingContext context)
        {
            this.name = (String)info.GetValue("Name", typeof(String));
            this.intervals = (List<Interval>)info.GetValue("Intervals", typeof(List<Interval>));
            this.duration = (TimeSpan)info.GetValue("Duration", typeof(TimeSpan));
        }

        // Przygotowanie składowych do serializacji.
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", this.name);
            info.AddValue("Intervals", this.intervals);
            info.AddValue("Duration", this.duration);
        }

        // Sprawdza, czy zadanie jest zatrzymane,
        // czyli czy ma rozpoczęty, a nie zakończony interwał.
        public bool isStopped()
        {
            if (intervals.Count == 0)
            {
                return true;
            }
            return intervals.Last().isStopped();
        }

        // Startowanie zadania (rozpoczęcie liczenia czasu).
        // Jeśli zadanie jest zatrzymane, tworzymy nowy interwał
        // z czasem startu równym 'teraz'. 
        public void start()
        {
            if (isStopped())
            {
                intervals.Add(new Interval());
            }
        }

        // Zatrzymywanie zadania.
        // Jeśli zadanie jest uruchomione, ustawiamy w ostatnim interwale
        // czas końca na 'teraz'. Zwiększamy całkowity czas trwania zadania
        // o długość właśnie zamkniętego interwału.
        public void stop()
        {
            if (!isStopped())
            {
                intervals.Last().stopIt();
                duration += intervals.Last().getDuration();
            }
        }

        // Zwraca nazwę zadania.
        public String getName()
        {
            return name;
        }

        // Zwraca listę interwałów.
        public List<Interval> getIntervals()
        {
            return intervals;
        }

        // Zwraca całkowity czas trwania zadania.
        // Jeśli zadanie jest zatrzymane, wystarczy zwrócić wartość
        // składowej duration. Dla zadań uruchomionych, musimy dodatkowo
        // dodać czas ostatniego, nie zakończonego interwału
        // (czas od startu interwału do 'teraz').
        public TimeSpan getDuration()
        {
            if (isStopped())
            {
                return duration;
            }
            return duration + intervals.Last().getDuration();
        }

        // Zwraca czas, przez jaki zadanie było uruchomione w bieżącym dniu.
        // Do wyniku dodajemy całkowity czas wszystkich interwałów, które rozpoczęły
        // się po północy bieżącego dnia. Jeśli znajdziemy również interwał,
        // który rozpoczął się przed północą, ale zakończył już w po północy
        // (lub jeszcze trwa) dodajemy do wyniku czas tego interwału liczony od północy.
        public TimeSpan getDurationToday()
        {
            TimeSpan dt = TimeSpan.Zero;
            DateTime today = DateTime.Today;
            int i = intervals.Count - 1;
            while (i >= 0 && intervals[i].getStart() >= today)
            {
                dt += intervals[i].getDuration();
                --i;
            }
            if (i >= 0)
            {
                if (intervals[i].getStop() > today)
                {
                    dt += intervals[i].getStop() - today;
                }
                else if (intervals[i].getStop() == DateTime.MinValue)
                {
                    dt += DateTime.Now - today;
                }
            }
            return dt;
        }

        // Przeciążona metoda ToString.
        // Z tej metody korzystają m.in. okna list, aby wyświetlić
        // przechowywane elementy. Zmieniamy domyślny sposób,
        // aby zadanie na liście pokazywane było jako: 
        // (dd.hh:mm:ss) nazwa zadania
        override public String ToString()
        {
            return "(" + duration.ToString(@"dd\.hh\:mm\:ss") + ") " + name;
        }
    }
}
