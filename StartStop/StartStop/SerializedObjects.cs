﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace StartStop
{
    // Pomocnicza klasa, grupująca obiekty, które chcemy zachować po zamknięciu aplikacji.
    // Dzieki niej wystarczy wykonać serializację jednego obiektu, aby zachować
    // wszystkie interesujące nas składowe.
    [Serializable()]
    class SerializedObjects : ISerializable
    {
        // Serializujemy listę zadań i atualne zadanie.
        public List<Task> tasks;
        public Task currentTask;

        public SerializedObjects() { }

        // Klasy implementujące interfejs ISerializable muszą posiadać taki konstruktor.
        // Używany on jest do zainicjowania obiektu przy pomocy deserializowanych danych.
        // Etykiety przekazywane do GetValue muszą pokrywać się z tymi użytymi w metodzie
        // GetObjectData.
        public SerializedObjects(SerializationInfo info, StreamingContext context)
        {
            this.tasks = (List<Task>)info.GetValue("Tasks", typeof(List<Task>));
            this.currentTask = (Task)info.GetValue("CurrentTask", typeof(Task));
        }

        // Metoda wymagana dla klas implementujących interfejs ISerializable.
        // Oznaczamy serializowane obiekty etykietami, które przy deserializacji posłużą
        // do ich zidentyfikowania.
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Tasks", this.tasks);
            info.AddValue("CurrentTask", this.currentTask);
        }
    }
}
